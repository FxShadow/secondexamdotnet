﻿using System.ComponentModel.DataAnnotations;

namespace ExamSecondTry.Models
{
    public class Employee
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage ="El documento es requerido")]
        public int Document { get; set; }

        [Required(ErrorMessage = "El nombre es requerido")]
        public String Name { get; set; }

        [Required(ErrorMessage = "El apellido es requerido")]
        public String Surname { get; set; }

        [Required(ErrorMessage = "La ciudad es requerida")]
        public String City { get; set; }

        [Required(ErrorMessage = "La direccion es requerida")]
        public String Address { get; set; }

        [Required(ErrorMessage = "La locacion es requerida")]
        public String Location { get; set; }

        [Required(ErrorMessage = "El correo es requerido")]
        public String Email { get; set; }

        [Required(ErrorMessage = "El telefono es requerido")]
        public int Phone { get; set; }
        
        public bool Status { get; set; } = true;
    }
}
