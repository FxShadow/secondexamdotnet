﻿using ExamSecondTry.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ExamSecondTry.Context
{
    public class ContextDb : IdentityDbContext
    {
        public ContextDb(DbContextOptions<ContextDb> options) : base(options) { }

        public DbSet<Employee> Employess { get; set; }
    }
}
